package models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Dairy {

	public int dairyID;
	public String dairyName;
		
	//Constructor
	public Dairy (){
		
	}
	
	//Method
	public static List<Dairy> getDairyNameData () {
		try{
			// create our mysql database connection
			Connection conn=DBConn.getconnection();
			
			// create the java statement
			Statement st = conn.createStatement();
	        
			//SQL STRING
			String sql  = "SELECT * FROM dairies" ;
			
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(sql);
	       
	        //create list product
	        List<Dairy> Dairies = new ArrayList<Dairy>();
	        
	        while(rs.next())
	        {		        
	        	Dairy Dairy =new Dairy();

	        	Dairy.dairyID = Integer.parseInt(rs.getString("dairyID"));
	        	Dairy.dairyName= rs.getString("dairyName");
	        	
	            Dairies.add(Dairy);
	        }
	        
	        //close recordset
	        rs.close();
	        
	        return Dairies;
	        
	    }
	    catch(Exception ex)
	    {
	    	System.out.println(ex.getMessage());
	    	return null;
	    }
		
	}
	
}
