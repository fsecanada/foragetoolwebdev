package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import models.Dairy;

/**
 * Servlet implementation class ReadDairyName
 */
@WebServlet("/DairyController")
public class DairyController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public DairyController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Load Model
		JSONArray output = new JSONArray();
		
		try{
			
			List<Dairy>  dairies = Dairy.getDairyNameData();
			for (Dairy dairy :dairies ){
				JSONObject item = new JSONObject();
				item.put("key", dairy.dairyID);
				item.put("value", dairy.dairyName);
				output.add(item);
			}			
			
		}catch(Exception ex){
				
			JSONObject item = new JSONObject();
			item.put("Error", ex.getMessage());
			output.add(item);
					
		}
						
		response.getWriter().append(output.toJSONString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
