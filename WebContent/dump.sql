-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-03-2017 a las 17:40:03
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `foragetool`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dairies`
--

CREATE TABLE `dairies` (
  `dairyID` int(11) NOT NULL,
  `dairyName` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dairies`
--

INSERT INTO `dairies` (`dairyID`, `dairyName`) VALUES
(1, 'DairyOne'),
(2, 'DairyTwo'),
(3, 'DairyThree'),
(4, 'DairyFour'),
(5, 'DairyFive');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `dairies`
--
ALTER TABLE `dairies`
  ADD PRIMARY KEY (`dairyID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `dairies`
--
ALTER TABLE `dairies`
  MODIFY `dairyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
